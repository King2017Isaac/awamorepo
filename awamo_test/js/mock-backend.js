var ajax = $.ajax;
$.ajax = function(config ) {
	var t = JSON.parse( config );
	
	var data = t.data,
		response;

	//alert(  data.opd1 + data.opd2  );
		
	switch (data.opn) {
		case 'ADD':
			response = data.opd1 + data.opd2;
			break;
		case 'SUB':
			response = data.opd1 - data.opd2;
			break;
		case 'MUL':
			response = data.opd1 * data.opd2;
			break;
		case 'DIV':
			response = data.opd1 / data.opd2;
			break;
	}
	if(Math.round(Math.random()) === 1) {
		response = Math.ceil(Math.random() * 2000);
	}

	//alert( response ); 

	config.success = response;
	return {
		fail: function() {}
	};
}
